class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    time = Time.new
    if time.hour < 12
      render html: "¡Good morning, world!"
    elsif time.hour >= 12 && time.hour < 17
      render html: "¡Good afternoon, world!"
    elsif time.hour >= 17 && time.hour <= 23
      render html: "¡Good evening, world!"
    else
      render html: "¡You should be in bed!"
    end
  end
end
